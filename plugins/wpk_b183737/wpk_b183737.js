(function( $ ) {
	'use strict';

	mpc_init_lightbox = function( $element, _is_gallery ) {
		var $lightbox = '',
			_vendor = '';

		if( $element.is( '.mpc-pretty-photo' ) || $element.find( '.mpc-pretty-photo' ).length ) {
			_vendor = 'prettyphoto';
		} else if( $element.is( '.mpc-magnific-popup' ) || $element.find( '.mpc-magnific-popup' ).length ) {
			_vendor = 'magnificPopup';
		}

		if( _vendor == '' ) {
			return;
		}

		if( $.fn.prettyPhoto && _vendor == 'prettyphoto' ) {
			$lightbox = $element.is( '.mpc-pretty-photo' ) ? $element : $element.find( '.mpc-pretty-photo' );

			$lightbox.prettyPhoto( {
				animationSpeed: 'normal',
				padding: 15,
				opacity: 0.7,
				showTitle: true,
				allowresize: false,
				hideflash: true,
				modal: false,
				social_tools: '',
				overlay_gallery: false,
				deeplinking: false,
				ie6_fallback: false
			} );
		} else if( $.fn.magnificPopup && _vendor == 'magnificPopup' ) {
			$lightbox = $element.is( '.mpc-magnific-popup' ) ? $element : $element.find( '.mpc-magnific-popup' );

			var _type = /(\.gif|\.jpg|\.jpeg|\.tiff|\.png|lightbox_src)/i.test( $lightbox.attr( 'href' ) ) ? 'image' : 'iframe',
				_atts;

			_atts = {
				type: _type,
				closeOnContentClick: true,
				mainClass:           'mfp-img-mobile',
				image:               {
					verticalFit: true,
					titleSrc: function( item ) {
						var $grid_item = item.el.closest( '.mpc-grid__image' ),
							_title     = '';

						if ( $grid_item.length ) {
							var $title = $grid_item.find( '.wpk_grid-image-title' ).eq( 0 );

							_title = $title.text();
						}

						return _title;
					}
				},
				callbacks:           {
					beforeOpen: function() {
						_mpc_vars.$window.trigger( 'mpc.lightbox.open' );
					},
					afterClose: function() {
						_mpc_vars.$window.trigger( 'mpc.lightbox.close' );
					}
				},
				iframe: {
					patterns:  {
						youtube: {
							id: function( _url ) {
								var _re = /https?:\/\/(?:[0-9A-Z-]+\.)?(?:youtu\.be\/|youtube(?:-nocookie)?\.com\S*?[^\w\s-])([\w-]{11})(?=[^\w-]|$)(?![?=&+%\w.-]*(?:['"][^<>]*>|<\/a>))([?=&+%\w.-]*)/ig;

								var _id =  _re.exec( _url );
								_url = typeof _id[ 1 ] !== typeof undefined ? _id[ 1 ] : '';

								if( _url !== '' && typeof _id[ 2 ] !== typeof undefined ) {
									_url += _id[ 2 ][ 0 ] == '&' ? _id[ 2 ].replace( '&', '?' ) : _id[ 2 ];
								}

								return _url;
							},
							src: '//www.youtube.com/embed/%id%?autoplay=1' // URL that will be set as a source for iframe.
						}
					}
				}
			};


			if( _is_gallery ) {
				_atts.gallery = {
					enabled: true,
					preload: [0,1],
					tCounter: ''
				}
			}

			$lightbox.magnificPopup( _atts );
		} else {
			setTimeout( mpc_init_lightbox( $element, _is_gallery ), 250 );
		}
	};

})( jQuery );