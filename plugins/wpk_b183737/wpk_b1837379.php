<?php
/*
Plugin Name: WP Kraken [#b183737]
Plugin URI: https://wpkraken.io/job/b183737/
Description: Customizing Job in Massive Addons / Grid Images
Author: WP Kraken [Mateusz]
Author URI: https://wpkraken.io/
Version: 1.0
Text Domain: wpk-b183737
*/

// require( 'helpers.php' );

if ( ! class_exists( 'WPK_b183737' ) ) {

	class WPK_b183737 {
		/** @var WPK_b183737 $instance */
		public static $instance;
		/** @var string $slug Contains plugin text domain */
		public static $slug = 'wpk-b183737';
		/** @var string $plugin_path Path to plugin directory */
		public static $plugin_path = '';
		/** @var string $plugin_url URL address to plugin directory */
		public static $plugin_url = '';

		/**
		 * WPK_b183737 constructor.
		 */
		public function __construct() {
			self::$instance = $this;

			if ( empty( self::$plugin_path ) ) {
				self::$plugin_path = plugin_dir_path( __FILE__ );
			}

			if ( empty( self::$plugin_url ) ) {
				self::$plugin_url = plugin_dir_url( __FILE__ );
			}

			/* Registers */
			add_action( 'plugins_loaded', array( $this, 'init' ), 1000 );
			add_filter( 'ma_grid_images_overlay', array( $this, 'gridItemTitle' ), 10, 2 );
		}

		/**
		 * Perform an initialization
		 */
		function init() {
			/* Registers */
			add_action( 'wp_enqueue_scripts', array( $this, 'registerScripts' ), 1000 );
		}


		/**
		 * Register Front End scripts and styles
		 */
		function registerScripts() {
			wp_enqueue_script( self::$slug, self::$plugin_url . 'wpk_b183737.js', array( 'jquery' ), null, true );
			wp_enqueue_style( self::$slug, self::$plugin_url . 'wpk_b183737.css', array(), null );
		}

		/**
		 * Create and return instance
		 *
		 * @return WPK_b183737
		 */
		public static function instance() {
			if ( self::$instance === null ) {
				self::$instance = new self();
			}

			return self::$instance;
		}

		function gridItemTitle( $overlay, $image_id ) {

			$output      = $overlay;
			$image_title = get_the_title( $image_id );

			if ( $image_title == '' ) {
				$image_title = get_post_meta( $image_id, '_wp_attachment_image_alt', true );
			}

			$output .= '<div class="wpk_grid-image-title mpc-typography--preset_1">' . $image_title . '</div>';

			return $output;
		}

	}
}

WPK_b183737::instance();